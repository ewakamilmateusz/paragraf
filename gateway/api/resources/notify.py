from elasticnotify import RequestError
from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from gateway.models import Notify, User
from gateway.extensions import ma, db, es

INDEX_NAME = "main"
QUERY = "query"
BOOL = "bool"
MUST = "must"
KEYWORDS = "keywords"
HIGHLIGHT = "highlight"
HIGHLIGHTS = "highlights"
URL = "URL"
PRE_TAGS = "pre_tags"
POST_TAGS = "post_tags"
FIELDS = "fields"
MULTI_MATCH = "multi_match"
FUZZINESS = "fuzziness"
FILTER = "filter"
TERMS = "terms"
SOURCE = "_source"
SHOULD = "should"
FRAGMENT_SIZE = "fragment_size"
NUMBER_OF_FRAGMENTS = "number_of_fragments"
TITLE = "title"
TEXT = "text"
NOTIFY = "notify"
SEARCH_FIELDS = [TEXT, TITLE]

HITS = "hits"


def create_path(json_entry):
    return "http://prawo.sejm.gov.pl/isap.nsf/download.xsp/{}/{}/{}".format(
        json_entry["address"],
        json_entry["texts"][0]["type"],
        json_entry["texts"][0]["fileName"],
    )


class NotifySchema(ma.ModelSchema):
    id = ma.Int(dump_only=True)
    notify_query = ma.Str(required=False)
    notify_tags = ma.Raw(required=False)
    user_id = ma.Int(required=False)
    notified = ma.Bool(required=False)
    last_update = ma.DateTime(required=False)

    class Meta:
        model = Notify
        sqla_session = db.session


class NotifyQueryBuilder(object):
    def __init__(self, notify_query, notify_tags, tags_only=False):
        self.tags_only = tags_only
        self.notify_query = notify_query
        self.notify_tags = notify_tags

    def get_query(self):
        if self.tags_only:
            return {
                QUERY: {
                    BOOL: {SHOULD: {TERMS: {KEYWORDS: self.notify_tags or []}}}
                }
            }
        return {
            QUERY: {
                BOOL: {
                    MUST: {
                        MULTI_MATCH: {
                            FIELDS: SEARCH_FIELDS,
                            QUERY: self.notify_query,
                            FUZZINESS: 1,
                        }
                    },
                    SHOULD: {TERMS: {KEYWORDS: self.notify_tags or []}},
                }
            },
            HIGHLIGHT: {
                PRE_TAGS: ["<b>"],
                POST_TAGS: ["</b>"],
                FIELDS: {
                    TITLE: {FRAGMENT_SIZE: 150, NUMBER_OF_FRAGMENTS: 1},
                    TEXT: {FRAGMENT_SIZE: 150, NUMBER_OF_FRAGMENTS: 1},
                },
            },
        }


class NotifyHistoryResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: notiy_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  notiy: NotifySchema
        404:
          description: notiy does not exists
    put:
      tags:
        - api
      parameters:
        - in: path
          name: notiy_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              NotifySchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  notiy: NotifySchema
        404:
          description: notify does not exists
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: notiy_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: notiy deleted
        404:
          description: notiy does not exists
    """

    method_decorators = [jwt_required]

    def get(self, notiy_id):
        schema = NotifySchema()
        notiy = Notify.query.get_or_404(notiy_id)
        return {"notiy": schema.dump(notiy)}

    def put(self, notiy_id):
        schema = NotifySchema(partial=True)
        notiy = Notify.query.get_or_404(notiy_id)
        notiy = schema.load(request.json, instance=notiy)

        db.session.commit()

        return {"msg": "notiy updated", "notiy": schema.dump(notiy)}

    def delete(self, notiy_id):
        notiy = Notify.query.get_or_404(notiy_id)
        db.session.delete(notiy)
        db.session.commit()

        return {"msg": "notiy deleted"}


class NotifyList(Resource):
    """Get notify results

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  hisrtory: NotifySchema
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              NotifySchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  results:
                    type: object
                    example: [{"URL":"https://www.goog.pl"}]
    """

    method_decorators = [jwt_required]

    def get(self):
        user_id = get_jwt_identity()
        user = User.query.get_or_404(user_id)
        schema = NotifySchema(many=True)
        notiy = schema.dump(user.notiy)
        return {"notiy": notiy}, 200

    def post(self):
        user_id = get_jwt_identity()
        schema = NotifySchema(partial=True)
        notify = schema.load(request.json)
        notify.user_id = user_id
        tags_only = False
        if not notify.notify_query or notify.notify_query in ["", None]:
            tags_only = True
        builder = NotifyQueryBuilder(
            notify.notify_query, notify.notify_tags, tags_only
        )
        query = builder.get_query()
        try:
            esresult = es.notify(index=INDEX_NAME, body=query)
        except RequestError:
            return {"result": []}, 200

        notify_user_list = []
        results = []
        if HITS in esresult.keys():
            for res in esresult[HITS][HITS]:
                notify_user_list.append(res[NOTIFY])
                results.append(
                    {
                        URL: create_path(res[SOURCE]),
                        HIGHLIGHTS: res[HIGHLIGHT] if not tags_only else [],
                        TITLE: res[SOURCE][TITLE],
                    }
                )

            if any(notify_user_list):
                notify.notified = False
            db.session.add(notify)
            db.session.commit()
            notify_objects = [y for x, y in zip(notify_user_list, results) if x]
            return {"result": results, "notify": notify_objects}, 200
        else:
            return {"result": []}, 200
