from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

from gateway.models import Metadata, User
from gateway.extensions import ma, db


class MetadataSchema(ma.ModelSchema):
    id = ma.Int(dump_only=True)
    tags = ma.Str(required=True)
    user_id = ma.Int(required=False)

    class Meta:
        model = Metadata
        sqla_session = db.session


class MetadataResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: metadata_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  metadatas: MetadataSchema
        404:
          description: metadata does not exists
    put:
      tags:
        - api
      parameters:
        - in: path
          name: metadata_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              MetadataSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  metadata: MetadataSchema
        404:
          description: metadata does not exists
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: metadata_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: metadata deleted
        404:
          description: metadata does not exists
    """

    method_decorators = [jwt_required]

    def get(self, metadata_id):
        schema = MetadataSchema()
        metadata = Metadata.query.get_or_404(metadata_id)
        return {"metadata": schema.dump(metadata)}

    def put(self, metadata_id):
        schema = MetadataSchema(partial=True)
        metadata = Metadata.query.get_or_404(metadata_id)
        metadata = schema.load(request.json, instance=metadata)

        db.session.commit()

        return {"msg": "metadata updated", "metadata": schema.dump(metadata)}

    def delete(self, metadata_id):
        metadata = Metadata.query.get_or_404(metadata_id)
        db.session.delete(metadata)
        db.session.commit()

        return {"msg": "metadata deleted"}


class MetadataList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: metadata updated
                  metadatas: MetadataSchema
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              UserSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: metadata created
                  metadata: MetadataSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        user_id = get_jwt_identity()
        user = User.query.get_or_404(user_id)
        schema = MetadataSchema(many=True)
        metadatas = schema.dump(user.metadatas)
        return {"metadatas": metadatas}, 200

    def post(self):
        user_id = get_jwt_identity()
        schema = MetadataSchema()
        metadata = schema.load(request.json)
        metadata.user_id = user_id
        db.session.add(metadata)
        db.session.commit()

        return (
            {"msg": "metadata created", "metadata": schema.dump(metadata)},
            201,
        )
