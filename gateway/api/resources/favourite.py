from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

from gateway.models import Favourite, User
from gateway.extensions import ma, db


class FavouriteSchema(ma.ModelSchema):

    id = ma.Int(dump_only=True)
    title = ma.Str(required=True)
    short_desc = ma.Str(required=False)
    user_id = ma.Int(required=False)

    class Meta:
        model = Favourite
        sqla_session = db.session


class FavouriteResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: favourite_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  favourites: FavouriteSchema
        404:
          description: favourite does not exists
    put:
      tags:
        - api
      parameters:
        - in: path
          name: favourite_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              FavouriteSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  favourite: FavouriteSchema
        404:
          description: user does not exists
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: favourite_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: favourite deleted
        404:
          description: favourite does not exists
    """

    method_decorators = [jwt_required]

    def get(self, favourite_id):
        schema = FavouriteSchema()
        favourite = Favourite.query.get_or_404(favourite_id)
        return {"favourite": schema.dump(favourite)}

    def put(self, favourite_id):
        schema = FavouriteSchema(partial=True)
        favourite = Favourite.query.get_or_404(favourite_id)
        favourite = schema.load(request.json, instance=favourite)

        db.session.commit()

        return {"msg": "favourite updated", "favourite": schema.dump(favourite)}

    def delete(self, favourite_id):
        favourite = Favourite.query.get_or_404(favourite_id)
        db.session.delete(favourite)
        db.session.commit()

        return {"msg": "favourite deleted"}


class FavouriteList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  favourites: FavouriteSchema
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              UserSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: favourite created
                  favourite: FavouriteSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        user_id = get_jwt_identity()
        user = User.query.get_or_404(user_id)
        schema = FavouriteSchema(many=True)
        favourites = schema.dump(user.favourites)
        return {"favourites": favourites}, 200

    def post(self):
        user_id = get_jwt_identity()
        schema = FavouriteSchema()
        favourite = schema.load(request.json)
        favourite.user_id = user_id
        db.session.add(favourite)
        db.session.commit()

        return (
            {"msg": "favourite created", "favourite": schema.dump(favourite)},
            201,
        )
