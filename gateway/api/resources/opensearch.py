from elasticsearch import RequestError
from flask import request
from flask_restful import Resource
from gateway.models import OpenSearch
from gateway.extensions import ma, db, es

INDEX_NAME = "main"
QUERY = "query"
BOOL = "bool"
MUST = "must"
KEYWORDS = "keywords"
HIGHLIGHT = "highlight"
HIGHLIGHTS = "highlights"
URL = "URL"
PRE_TAGS = "pre_tags"
POST_TAGS = "post_tags"
FIELDS = "fields"
MULTI_MATCH = "multi_match"
FUZZINESS = "fuzziness"
FILTER = "filter"
TERMS = "terms"
SOURCE = "_source"
SHOULD = "should"
FRAGMENT_SIZE = "fragment_size"
NUMBER_OF_FRAGMENTS = "number_of_fragments"
TITLE = "title"
TEXT = "text"
SEARCH_FIELDS = [TEXT, TITLE]

HITS = "hits"


def create_path(json_entry):
    return "http://prawo.sejm.gov.pl/isap.nsf/download.xsp/{}/{}/{}".format(
        json_entry["address"],
        json_entry["texts"][0]["type"],
        json_entry["texts"][0]["fileName"],
    )


class OpenSearchSchema(ma.ModelSchema):
    id = ma.Int(dump_only=True)
    search_query = ma.Str(required=False)
    search_tags = ma.Raw(required=False)

    class Meta:
        model = OpenSearch
        sqla_session = db.session


class SearchQueryBuilder(object):
    def __init__(self, search_query, search_tags, tags_only=False):
        self.tags_only = tags_only
        self.search_query = search_query
        self.search_tags = search_tags

    def get_query(self):
        if self.tags_only:
            return {
                QUERY: {
                    BOOL: {SHOULD: {TERMS: {KEYWORDS: self.search_tags or []}}}
                }
            }
        return {
            QUERY: {
                BOOL: {
                    MUST: {
                        MULTI_MATCH: {
                            FIELDS: SEARCH_FIELDS,
                            QUERY: self.search_query,
                            FUZZINESS: 1,
                        }
                    },
                    SHOULD: {TERMS: {KEYWORDS: self.search_tags or []}},
                }
            },
            HIGHLIGHT: {
                PRE_TAGS: ["<b>"],
                POST_TAGS: ["</b>"],
                FIELDS: {
                    TITLE: {FRAGMENT_SIZE: 150, NUMBER_OF_FRAGMENTS: 1},
                    TEXT: {FRAGMENT_SIZE: 150, NUMBER_OF_FRAGMENTS: 1},
                },
            },
        }


class OpenSearchList(Resource):
    """Get search results

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  hisrtory: SearchSchema
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              SearchSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  results:
                    type: object
                    example: [{"URL":"https://www.goog.pl"}]
    """

    def post(self):
        schema = OpenSearchSchema(partial=True)
        search = schema.load(request.json)
        tags_only = False
        if not search.search_query or search.search_query in ["", None]:
            tags_only = True
        builder = SearchQueryBuilder(
            search.search_query, search.search_tags, tags_only
        )
        query = builder.get_query()
        try:
            esresult = es.search(index=INDEX_NAME, body=query)
        except RequestError:
            return {"result": []}, 200
        db.session.add(search)
        db.session.commit()
        results = []
        if HITS in esresult.keys():
            for res in esresult[HITS][HITS]:
                results.append(
                    {
                        URL: create_path(res[SOURCE]),
                        HIGHLIGHTS: res[HIGHLIGHT] if not tags_only else [],
                        TITLE: res[SOURCE][TITLE],
                    }
                )
            return {"result": results}, 200
        else:
            return {"result": []}, 200
