from flask import Blueprint, current_app, jsonify
from flask_restful import Api
from marshmallow import ValidationError

from gateway.api.resources.opensearch import OpenSearchSchema
from gateway.api.resources.search import SearchSchema
from gateway.extensions import apispec
from gateway.api.resources import (
    UserResource,
    UserList,
    FavouriteResource,
    FavouriteList,
    MetadataResource,
    MetadataList,
    SearchList,
    OpenSearchList,
    HistoryResource,
    NotifyList, NotifyHistoryResource)
from gateway.api.resources.user import UserSchema
from gateway.api.resources.favourite import FavouriteSchema
from gateway.api.resources.metadata import MetadataSchema

blueprint = Blueprint("api", __name__, url_prefix="/api/v1")
api = Api(blueprint)

api.add_resource(UserResource, "/users/<int:user_id>")
api.add_resource(UserList, "/users")

api.add_resource(FavouriteResource, "/favourite/<int:favourite_id>")
api.add_resource(FavouriteList, "/favourite")

api.add_resource(MetadataResource, "/metadata/<int:metadata_id>")
api.add_resource(MetadataList, "/metadata")

api.add_resource(HistoryResource, "/history/<int:history_id>")
api.add_resource(SearchList, "/search")

api.add_resource(NotifyHistoryResource, "/notifyhistory/<int:history_id>")
api.add_resource(NotifyList, "/notify")

api.add_resource(OpenSearchList, "/opensearch")


@blueprint.before_app_first_request
def register_views():
    apispec.spec.components.schema("UserSchema", schema=UserSchema)
    apispec.spec.path(view=UserResource, app=current_app)
    apispec.spec.path(view=UserList, app=current_app)

    apispec.spec.components.schema("FavouriteSchema", schema=FavouriteSchema)
    apispec.spec.path(view=FavouriteResource, app=current_app)
    apispec.spec.path(view=FavouriteList, app=current_app)

    apispec.spec.components.schema("MetadataSchema", schema=MetadataSchema)
    apispec.spec.path(view=MetadataResource, app=current_app)
    apispec.spec.path(view=MetadataList, app=current_app)

    apispec.spec.components.schema("SearchSchema", schema=SearchSchema)
    apispec.spec.path(view=HistoryResource, app=current_app)
    apispec.spec.path(view=SearchList, app=current_app)

    apispec.spec.components.schema("OpenSearchSchema", schema=OpenSearchSchema)
    apispec.spec.path(view=OpenSearchList, app=current_app)

    apispec.spec.components.schema("NotifySchema", schema=NotifySchema)
    apispec.spec.path(view=NotifyHistoryResource, app=current_app)
    apispec.spec.path(view=NotifyList, app=current_app)


@blueprint.errorhandler(ValidationError)
def handle_marshmallow_error(e):
    """Return json error for marshmallow validation errors.

    This will avoid having to try/catch ValidationErrors in all endpoints,
    returning correct JSON response with associated
    HTTP 400 Status (https://tools.ietf.org/html/rfc7231#section-6.5.1)
    """
    return jsonify(e.messages), 400
