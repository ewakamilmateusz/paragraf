from gateway.extensions import db


class OpenSearch(db.Model):
    """OpenSearch object
    """

    id = db.Column(db.Integer, primary_key=True)
    search_query = db.Column(db.String(1500), nullable=True)
    search_tags = db.Column(db.Text, nullable=True)

    def __init__(self, **kwargs):
        super(OpenSearch, self).__init__(**kwargs)

    def __repr__(self):
        return "<OpenSearch %s>" % self.id
