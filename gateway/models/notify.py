from gateway.extensions import db


class Notify(db.Model):
    """Notify object
    """

    id = db.Column(db.Integer, primary_key=True)
    search_query = db.Column(db.String(1500), nullable=True)
    search_tags = db.Column(db.Text, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    notified = db.Column(db.Boolean, default=False)
    last_update = db.Column(db.DateTime)

    def __init__(self, **kwargs):
        super(Notify, self).__init__(**kwargs)

    def __repr__(self):
        return "<Notify %s>" % self.id
