from gateway.extensions import db


class Search(db.Model):
    """Search object for user
    Will be used for user search history
    """

    id = db.Column(db.Integer, primary_key=True)
    search_query = db.Column(db.String(1500), nullable=True)
    search_tags = db.Column(db.Text, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __init__(self, **kwargs):
        super(Search, self).__init__(**kwargs)

    def __repr__(self):
        return "<Search %s>" % self.id
