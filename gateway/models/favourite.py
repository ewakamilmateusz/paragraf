from gateway.extensions import db


class Favourite(db.Model):
    """Favourite object for user
    """

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=True)
    short_desc = db.Column(db.Text, nullable=True)
    url = db.Column(db.String(255), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __init__(self, **kwargs):
        super(Favourite, self).__init__(**kwargs)

    def __repr__(self):
        return "<Favourite %s>" % self.title
