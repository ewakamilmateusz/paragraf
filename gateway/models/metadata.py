from gateway.extensions import db


class Metadata(db.Model):
    """Metadata object for user
    """

    id = db.Column(db.Integer, primary_key=True)
    tags = db.Column(db.Text, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __init__(self, **kwargs):
        super(Metadata, self).__init__(**kwargs)

    def __repr__(self):
        return "<Metadata %s>" % self.id
