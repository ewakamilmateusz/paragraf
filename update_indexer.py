import pickle
from elasticsearch import Elasticsearch

es = Elasticsearch("http://localhost:9200")
INDEX_NAME = "main"

with open('recently_changed.pkl', 'rb') as dump_file:
    recent_cahnges = pickle.loads(dump_file.read())

for doc in recent_cahnges:
    doc["text"] = doc["text"].replace("\n", "")
    doc['notify'] = 1
    es.update(index=INDEX_NAME, id=doc["address"], body=doc, refresh=False)
