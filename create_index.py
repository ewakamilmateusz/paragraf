import json

import pandas as pd
from elasticsearch import Elasticsearch

es = Elasticsearch("http://localhost:9200")
INDEX_NAME = "main"

if es.indices.exists(INDEX_NAME):
    es.indices.delete(INDEX_NAME)


def create_index():
    settings = {
        "settings": {
            "analysis": {
                "analyzer": {
                    "lang_pl": {
                        "type": "custom",
                        "tokenizer": "standard",
                        "filter": ["lowercase", "polish_stem"],
                    },
                    "ngram_analyzer": {
                        "type": "custom",
                        "tokenizer": "ngram_tokenizer",
                        "filter": ["lowercase", "polish_stem"],
                    },
                },
                "tokenizer": {
                    "ngram_tokenizer": {
                        "type": "edge_ngram",
                        "min_gram": 3,
                        "max_gram": 10,
                        "token_chars": ["letter", "digit"],
                    }
                },
            }
        },
        "mappings": {
            "properties": {
                "address": {"type": "text"},
                "publisher": {"type": "text"},
                "year": {"type": "integer"},
                "volume": {"type": "integer"},
                "pos": {"type": "integer"},
                "type": {"type": "text"},
                "title": {"type": "text"},
                "status": {"type": "text"},
                "announcementDate": {"type": "text"},
                "changeDate": {"type": "text"},
                "text": {"type": "text"},
                "keywords": {"type": "text", "boost": 1.4},
            }
        },
    }

    es.indices.create(index=INDEX_NAME, body=settings)


def get_text_df():
    file_path2019 = r"/home/pweb_admin/paragraf/scraped/2019_all.json"
    file_path2020 = r"/home/pweb_admin/paragraf/scraped/2020_full.json"
    df2019 = pd.read_json(file_path2019)
    df2020 = pd.read_json(file_path2020)
    df = pd.concat([df2019, df2020])
    return df.loc[~df.text.str.contains("null")]


def get_json_data(dataframe):
    return json.loads(dataframe.to_json(orient="records", date_format="iso"))


def populate():
    create_index()
    df = get_text_df()
    data = get_json_data(df)
    for i, doc in enumerate(data):
        doc["text"] = doc["text"].replace("\n", "")
        es.index(index=INDEX_NAME, id=doc["address"], body=doc, refresh=False)


populate()
