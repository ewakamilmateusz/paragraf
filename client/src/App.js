import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Layout from './components/layout/Layout'

export default function App() {
  return (
    <Container maxWidth="sm">
      <Box xs={12} xl={12}>
        < Layout />
      </Box>
    </Container>
  );
}
