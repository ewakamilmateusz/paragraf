import React, { Component } from "react";
import { withStyles, withTheme, useTheme } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Link from "@material-ui/core/Link";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import ReactHtmlParser from "react-html-parser";
const styles = {
  root: {
    width: "100%",
    maxWidth: "67ch",
  },
  inline: {
    display: "inline",
  },
  tooltip: {
    color: "rgba(0, 0, 0, 0.87)",
    fontSize: 13,
  },
};

const HtmlTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: "#f5f5f9",
    color: "rgba(0, 0, 0, 0.87)",
    maxWidth: 350,
    border: "1px solid #dadde9",
    fontSize: 16,
  },
}))(Tooltip);

class ResultList extends Component {
  render() {
    const { classes, data } = this.props;
    const preventDefault = (event) => event.preventDefault();
    return (
      <List className={classes.root}>
        {data.map((element) => (
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <Avatar alt={element.title} src="/static/images/avatar/1.jpg" />
            </ListItemAvatar>
            <HtmlTooltip
              placement="right"
              arrow
              title={
                ReactHtmlParser(element.highlights.title
                  ? element.highlights.title
                  : element.highlights.text)
              }
            >
              <ListItemText
                primary={element.title}
                secondary={
                  <React.Fragment>
                    <Typography
                      component="span"
                      variant="body1"
                      className={classes.inline}
                      color="textPrimary"
                    >
                      <Link href={element.URL} >
                        Pobierz ustawę
                      </Link>
                    </Typography>
                  </React.Fragment>
                }
              />
            </HtmlTooltip>
          </ListItem>
        ))}
      </List>
    );
  }
}

export default withStyles(styles)(withTheme(ResultList));
