import React, { Component } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import axios from "axios";
import TextField from "@material-ui/core/TextField";

const styles = {
  textField: {
    width: "55ch",
  },
};

export const headerConfig = () => {
  const config = {
    headers: {
      "Content-type": "application/json",
    },
  };
  return config;
};

export const getResults = async (search_query, search_tags) => {
  return await axios
    .post(
      "/api/v1/opensearch",
      { search_query, search_tags },
      headerConfig()
    )
    .then((res) => {
      return res.data.result;
    })
    .catch((err) => console.log(err));
};

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textFieldValue: null,
      results: null,
    };
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    const response = getResults(this.state.textFieldValue, "");
    const data = await response;
    this.setState({ results: data });
    this.props.callbackFromLayout(this.state.results);
  };

  handleTextFieldChange = (e) => {
    this.setState({
      textFieldValue: e.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <form
          className={classes.textField}
          noValidate
          onSubmit={this.handleSubmit}
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Zapytanie"
            variant="outlined"
            onChange={this.handleTextFieldChange}
            fullWidth
            style={{ margin: 8 }}
          />
        </form>
      </div>
    );
  }
}

export default withStyles(styles)(withTheme(SearchBar));
