import React, { Component } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import SearchBar from "../common/SearchBar";
import ResultList from "../common/ResultList";
import Typography from "@material-ui/core/Typography";

import paragraf from "../../images/paragraf.png";

const styles = {
  root: {
    flexGrow: 1,
    justifyContent: "space-around",
  },
};

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      display: null,
    };
  }
  callback = (dataFromSearch) => {
    this.setState({ data: dataFromSearch, display: "Brak wyników" });
  };
  render() {
    return (
      <Grid container spacing={3}>
        <Grid item>
          <img src={paragraf} alt="fireSpot" height="100px" width="100px" />
        </Grid>
        <Grid item>
          <Typography variant="h1" component="h1" gutterBottom>
            Paragraf
          </Typography>
        </Grid>
        <Grid item>
          <Container maxWidth="md">
            <SearchBar callbackFromLayout={this.callback} />
            {this.state.data ? (
              <ResultList data={this.state.data} />
            ) : (
              <Typography component="span" variant="body1" color="textPrimary">
                {this.state.display}
              </Typography>
            )}
          </Container>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(withTheme(Layout));
