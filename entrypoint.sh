#!/bin/sh

if [ "$1" = 'server' ]; then
    export FLASK_ENV=production
    export FLASK_APP=gateway.app:create_app
    gateway db upgrade
    gateway init
    gunicorn -w 2 -b 0.0.0.0:5000 gateway.wsgi:app --preload
fi
