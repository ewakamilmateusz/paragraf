import json
import pickle
from typing import Dict

import textract

import requests
from datetime import datetime, timedelta
from pathlib import Path

last_update_date = datetime(year=2020, month=4, day=3)

CHANGES_ENDPOINT = "http://isap.sejm.gov.pl/api/isap/changes/deeds"
MAX_RESPONSE_LENGTH = 300000


def get_recent_updates(date_since: datetime = None) -> Dict:
    if date_since is None:
        date_since = datetime.now() - timedelta(days=1)
    output = requests.get(CHANGES_ENDPOINT, params={"since": date_since})
    return json.loads(output.content, encoding="utf-8")["items"]


def create_download_path(json_entry):
    file_info = get_entry_file_info(json_entry)
    return "http://prawo.sejm.gov.pl/isap.nsf/download.xsp/{}/{}/{}".format(
        json_entry["address"], file_info["type"], file_info["fileName"]
    )


def get_entry_file_info(json_entry):
    for entry in json_entry["texts"]:
        if entry["fileName"][-3:] == "pdf":
            return entry
    return None


def get_text_for_document(json_entry: Dict):
    download_url = create_download_path(json_entry=json_entry)
    output = requests.get(download_url)
    if len(output.content) > MAX_RESPONSE_LENGTH:
        return ""
    filename = Path("temp_file.pdf")
    filename.write_bytes(output.content)
    text = clean_text(
        textract.process(
            "metadata.pdf", method="tesseract", language="pol"
        ).decode("utf-8")
    )
    return text


def clean_text(text):
    text = text.replace("\n", "")
    return text


def update_json_entry_text(json_entry):
    json_entry["text"] = get_text_for_document(json_entry)
    return json_entry


def update_recently_changed(changed_since: datetime):
    updated_docs = get_recent_updates(changed_since)
    updated_docs_with_text = []
    for entry in updated_docs:
        updated_docs_with_text.append(update_json_entry_text(entry))
    return updated_docs_with_text


with open('recently_changed.pkl', 'wb') as dump_file:
    dump_file.write(pickle.dumps(update_recently_changed(last_update_date)))
