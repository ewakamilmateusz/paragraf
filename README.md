# Paragraf - REST API 
Repository of Paragraf Mobile App backend environment and Paragraf Search Web App frontend environment.

Contains:
- REST API
- Cron Jobs
- ElastiSearch handlers
- Paragraf Search Web App frontent repository
- Nginx configuration
- CI/CD configuration

## For Users
### You can view Paragraf Search here: [http://40.118.124.191](http://40.118.124.191)
### To access documentation visit: [http://40.118.124.191/docs](http://40.118.124.191/docs)

## Technology
- Docker, all services are deployed on separate containers
- Flask
- ElasticSearch
- Tesseract
- React
- Nginx as reverse proxy

## For Developers
### To run this localy you will need [Docker](https://docs.docker.com/get-docker/)

### Run localy
```bash
export DB_HOST postgres-db
export POSTGRES_DB gateway
export POSTGRES_USER postgres
export POSTGRES_PASSWORD secretPassword321
export SECRET_KEY SecretKey444
export ADMIN_PASS admin

docker-compose up --build
```
#### Open localhost to access API
